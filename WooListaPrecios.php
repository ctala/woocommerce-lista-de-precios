<?php

/*
  Plugin Name: WooCommerce Lista de Precios
  Plugin URI:  https://github.com/ctala/wp-skeleton
  Description: Descarga en formato excel la lista de precios de todos los productos de la tienda
  Version:     1.0
  Author:      Cristian Tala Sánchez
  Author URI:  http://www.cristiantala.cl
  License:     MIT
  License URI: http://opensource.org/licenses/MIT
  Domain Path: /languages
  Text Domain: ctala-text_domain
 */

function getMiListaPrecios() {

    if (!isset($_GET['descargarListaPrecios']))
        return true;


    $lPrecios = new MiListaDePrecios();
    $resultado = $lPrecios->getListaDePrecios();
    $lPrecios->getExcel($resultado);
}

add_action('admin_init', 'getMiListaPrecios');

class MiListaDePrecios {

    function getListaDePrecios() {
        global $wpdb;
        $result = $wpdb->get_results("
                    SELECT * 
                    FROM  $wpdb->posts
                    WHERE post_type = 'product'
                    AND
                    post_status='publish'
                ");
        return $result;
    }

    function getSKUProducto($id) {
        global $wpdb;
        $tabla = $wpdb->prefix . "postmeta";
        $SKU = $wpdb->get_var(
                "SELECT meta_value "
                . "FROM $tabla "
                . "WHERE "
                . "meta_key = '_sku' "
                . "AND "
                . "post_id = $id");
        return $SKU;
    }

    function getPriceProducto($id) {
        global $wpdb;
        $tabla = $wpdb->prefix . "postmeta";
        $price = $wpdb->get_var(
                "SELECT meta_value "
                . "FROM $tabla "
                . "WHERE "
                . "meta_key = '_regular_price' "
                . "AND "
                . "post_id = $id");
        return $price;
    }

    function getPriceRebajadoProducto($id) {
        global $wpdb;
        $tabla = $wpdb->prefix . "postmeta";
        $price = $wpdb->get_var(
                "SELECT meta_value "
                . "FROM $tabla "
                . "WHERE "
                . "meta_key = '_sale_price' "
                . "AND "
                . "post_id = $id");
        return $price;
    }

    function getExcel($productos) {
        require 'vendor/autoload.php';
        $objPHPExcel = new PHPExcel();
        $date = new DateTime();
        $timeStamp = $date->getTimestamp();

        // Fila que se repite
        $objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 1);

        //Titulos para el Excel
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', "ID")
                ->setCellValue('B1', "SKU")
                ->setCellValue('C1', 'Nombre')
                ->setCellValue('D1', 'Precio Normal')
                ->setCellValue('E1', 'Precio Rebajado');


        $fila = 2;
        foreach ($productos as $producto) {
            $id = $producto->ID;
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $fila, $id)
                    ->setCellValue('B' . $fila, $this->getSKUProducto($id))
                    ->setCellValue('C' . $fila, $producto->post_title)
                    ->setCellValue('D' . $fila, $this->getPriceProducto($id))
                    ->setCellValue('E' . $fila, $this->getPriceRebajadoProducto($id));
            $fila++;
        }

        //Ya teniendo todos los productos, actualizamos el ancho de las columnas
        foreach (range('A', 'B', 'C', 'D', 'E', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
        }

        $objPHPExcel->getActiveSheet()->setTitle('ListaPreciosWooCommerce');

        $objPHPExcel->setActiveSheetIndex(0);

        ob_end_clean();
        ob_start();

        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=\"PRECIOS_$timeStamp.xls\"");
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

}

add_action('admin_menu', 'ctala_setup_admin_menu');

function ctala_setup_admin_menu() {
    if (empty($GLOBALS['admin_page_hooks']['ctala_admin'])) {
        add_menu_page(
                'Descarga La Lista de Precios de WooCommerce', 'Extra Tools', 'manage_options', 'ctala_admin', 'ctala_view_admin');
    }

    add_submenu_page('ctala_admin', 'Descargar Lista', 'Descargar Lista', 'manage_options', 'descargarLista', 'ctala_view_admin');
}

function ctala_view_admin() {
    include_once 'views/admin/viewAdmin.php';
}

?>
